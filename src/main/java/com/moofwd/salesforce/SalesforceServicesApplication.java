package com.moofwd.salesforce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesforceServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesforceServicesApplication.class, args);
	}

}
