package com.moofwd.salesforce.controllers;

import com.moofwd.salesforce.request.CaseIdRequest;
import com.moofwd.salesforce.request.cases.comments.AddCaseCommentRequest;
import com.moofwd.salesforce.request.cases.comments.CaseCommentRequest;
import com.moofwd.salesforce.request.cases.feeds.FeedItemRequest;
import com.moofwd.salesforce.request.contacs.ContactRequest;
import com.moofwd.salesforce.request.cases.CreateCaseRequest;
import com.moofwd.salesforce.request.documents.AddDocumentRequest;
import com.moofwd.salesforce.response.cases.CasesResponse;
import com.moofwd.salesforce.response.CreateResponse;
import com.moofwd.salesforce.response.cases.comments.CaseCommentResponse;
import com.moofwd.salesforce.response.cases.feeds.FeedItemResponse;
import com.moofwd.salesforce.response.documents.DocumentResponse;
import com.moofwd.salesforce.services.CaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cases")
public class CaseController {
    private static Logger log = LoggerFactory.getLogger(CaseController.class);
    private static final String PRODUCES_JSON_STRING = "application/json; charset=utf-8";

    @Autowired
    CaseService caseService;

    @PostMapping(path = "list", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<CasesResponse> getCasesList(@RequestBody ContactRequest request){
        try {
            CasesResponse response = caseService.getCases(request);
            return new ResponseEntity<CasesResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<CasesResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "create", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<CreateResponse> createCase(@RequestBody CreateCaseRequest request){
        try {
            CreateResponse response = caseService.createCase(request);
            return new ResponseEntity<CreateResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<CreateResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "attachment", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<CreateResponse> addAttachment(@RequestBody AddDocumentRequest request){
        try {
            CreateResponse response = caseService.addAttachment(request);
            return new ResponseEntity<CreateResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<CreateResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "attachments", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<DocumentResponse> getAttachements(@RequestBody CaseIdRequest request){
        try {
            DocumentResponse response = caseService.getAttachements(request);
            return new ResponseEntity<DocumentResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<DocumentResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "comments", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<CaseCommentResponse> getCaseComments(@RequestBody CaseCommentRequest request){
        try {
            CaseCommentResponse response = caseService.getCaseCommentsByCaseId(request);
            return new ResponseEntity<CaseCommentResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<CaseCommentResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "feeds", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<FeedItemResponse> get(@RequestBody FeedItemRequest request){
        try {
            FeedItemResponse response = caseService.getFeedItemsByCaseId(request);
            return new ResponseEntity<FeedItemResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<FeedItemResponse>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "comment", consumes = "application/json", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<CreateResponse> addComment(@RequestBody AddCaseCommentRequest request){
        try {
            CreateResponse response = caseService.addCaseComment(request);
            return new ResponseEntity<CreateResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<CreateResponse>(HttpStatus.BAD_REQUEST);
        }
    }

}
