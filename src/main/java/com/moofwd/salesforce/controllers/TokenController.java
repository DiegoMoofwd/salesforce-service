package com.moofwd.salesforce.controllers;

import com.moofwd.salesforce.response.LoginResponse;
import com.moofwd.salesforce.services.CaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
public class TokenController {

    private static Logger log = LoggerFactory.getLogger(CaseController.class);
    private static final String PRODUCES_JSON_STRING = "application/json; charset=utf-8";

    @Autowired
    CaseService caseService;

    @GetMapping(value="generate", produces = PRODUCES_JSON_STRING)
    public ResponseEntity<LoginResponse> generateToken() {
        try {
            LoginResponse response = caseService.getLoginResponse();
            return new ResponseEntity<LoginResponse>(response, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<LoginResponse>(HttpStatus.BAD_REQUEST);
        }
    }

}
