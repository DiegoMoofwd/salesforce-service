package com.moofwd.salesforce.models.cases;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moofwd.salesforce.models.Attributes;
import com.moofwd.salesforce.response.cases.comments.CaseCommentResponse;

public class Case {
    private Attributes attributes;

    @JsonProperty("Id")
    private String id;

    @JsonProperty("CreatedById")
    private String createdById;

    @JsonProperty("AccountId")
    private String accountId;

    @JsonProperty("ParentId")
    private String parentId;

    @JsonProperty("CaseNumber")
    private String caseNumber;

    @JsonProperty("CreatedDate")
    private String createdDate;

    @JsonProperty("Origin")
    private String origin;

    @JsonProperty("Reason")
    private String reason;

    @JsonProperty("Type")
    private String type;

    @JsonProperty("IsClosed")
    private boolean isClosed;

    @JsonProperty("IsEscalated")
    private boolean isEscalated;

    @JsonProperty("ClosedDate")
    private String closedDate;

    @JsonProperty("Subject")
    private String subject;

    @JsonProperty("Description")
    private String description;

    //@JsonProperty("Comments")

    @JsonProperty("ContactId")
    private String contactId;

    @JsonProperty("ContactEmail")
    private String contactEmail;

    //@JsonProperty("SuppliedEmail")

    //@JsonProperty("SuppliedName")

    @JsonProperty("Status")
    private String status;

    @JsonProperty("CaseComments")
    private CaseCommentResponse caseComments;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(String caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public boolean isEscalated() {
        return isEscalated;
    }

    public void setEscalated(boolean escalated) {
        isEscalated = escalated;
    }

    public String getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(String closedDate) {
        this.closedDate = closedDate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CaseCommentResponse getCaseComments() {
        return caseComments;
    }

    public void setCaseComments(CaseCommentResponse caseComments) {
        this.caseComments = caseComments;
    }
}
