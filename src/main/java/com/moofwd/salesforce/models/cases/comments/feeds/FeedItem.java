package com.moofwd.salesforce.models.cases.comments.feeds;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moofwd.salesforce.models.Attributes;
import com.moofwd.salesforce.response.cases.feeds.FeedCommentsResponse;

public class FeedItem {
    private Attributes attributes;

    @JsonProperty("Id")
    private String id;

    @JsonProperty("CreatedDate")
    private String createdDate;

    @JsonProperty("CreatedById")
    private String createdById;

    @JsonProperty("FeedComments")
    private FeedCommentsResponse feedComments;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public FeedCommentsResponse getFeedComments() {
        return feedComments;
    }

    public void setFeedComments(FeedCommentsResponse feedComments) {
        this.feedComments = feedComments;
    }
}
