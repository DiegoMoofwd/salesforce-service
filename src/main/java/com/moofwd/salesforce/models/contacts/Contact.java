package com.moofwd.salesforce.models.contacts;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moofwd.salesforce.models.Attributes;

public class Contact {
    private Attributes attributes;

    @JsonProperty("Id")
    private String id;

    @JsonProperty("AccountId")
    private String accountId;

    @JsonProperty("Email")
    private String email;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
