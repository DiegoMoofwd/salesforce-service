package com.moofwd.salesforce.models.documents;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moofwd.salesforce.models.Attributes;

public class ContentDocumentId {
    private Attributes attributes;

    @JsonProperty("ContentDocumentId")
    private String contentDocumentId;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getContentDocumentId() {
        return contentDocumentId;
    }

    public void setContentDocumentId(String contentDocumentId) {
        this.contentDocumentId = contentDocumentId;
    }
}
