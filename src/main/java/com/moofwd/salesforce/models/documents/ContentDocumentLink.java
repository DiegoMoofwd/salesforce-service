package com.moofwd.salesforce.models.documents;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moofwd.salesforce.models.Attributes;

public class ContentDocumentLink {
    private Attributes attributes;

    @JsonProperty("ContentDocumentId")
    private String contentDocumentId;

    @JsonProperty("ContentDocument")
    private ContentDocument contentDocument;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getContentDocumentId() {
        return contentDocumentId;
    }

    public void setContentDocumentId(String contentDocumentId) {
        this.contentDocumentId = contentDocumentId;
    }

    public ContentDocument getContentDocument() {
        return contentDocument;
    }

    public void setContentDocument(ContentDocument contentDocument) {
        this.contentDocument = contentDocument;
    }
}
