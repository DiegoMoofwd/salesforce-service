package com.moofwd.salesforce.models.documents;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moofwd.salesforce.models.Attributes;

public class VersionData {
    private Attributes attributes;

    @JsonProperty("VersionData")
    private String versionData;

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public String getVersionData() {
        return versionData;
    }

    public void setVersionData(String versionData) {
        this.versionData = versionData;
    }
}
