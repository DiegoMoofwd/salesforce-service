package com.moofwd.salesforce.request.cases.comments;

public class CaseCommentRequest {
    private String token;
    private String parentId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
