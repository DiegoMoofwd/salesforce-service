package com.moofwd.salesforce.request.documents;

public class AddDocumentRequest {
    private String token;
    private String caseId;
    private String title;
    private String pathOnClient;
    private String contentLocation;
    private String versionData;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPathOnClient() {
        return pathOnClient;
    }

    public void setPathOnClient(String pathOnClient) {
        this.pathOnClient = pathOnClient;
    }

    public String getContentLocation() {
        return contentLocation;
    }

    public void setContentLocation(String contentLocation) {
        this.contentLocation = contentLocation;
    }

    public String getVersionData() {
        return versionData;
    }

    public void setVersionData(String versionData) {
        this.versionData = versionData;
    }
}
