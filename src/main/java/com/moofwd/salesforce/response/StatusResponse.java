package com.moofwd.salesforce.response;

public class StatusResponse {
    private String status;
    private String message;

    public StatusResponse() {
        this.status = "500";
        this.message = "Error en el servicio solicitado";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
