package com.moofwd.salesforce.response.cases;

import com.moofwd.salesforce.models.cases.Case;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class CasesResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<Case> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<Case> getRecords() {
        return records;
    }

    public void setRecords(List<Case> records) {
        this.records = records;
    }
}
