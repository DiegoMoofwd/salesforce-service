package com.moofwd.salesforce.response.cases.comments;

import com.moofwd.salesforce.models.cases.comments.CaseComment;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class CaseCommentResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<CaseComment> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<CaseComment> getRecords() {
        return records;
    }

    public void setRecords(List<CaseComment> records) {
        this.records = records;
    }
}
