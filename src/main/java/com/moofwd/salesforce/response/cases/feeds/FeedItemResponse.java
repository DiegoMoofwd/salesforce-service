package com.moofwd.salesforce.response.cases.feeds;

import com.moofwd.salesforce.models.cases.comments.feeds.FeedItem;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class FeedItemResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<FeedItem> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<FeedItem> getRecords() {
        return records;
    }

    public void setRecords(List<FeedItem> records) {
        this.records = records;
    }
}
