package com.moofwd.salesforce.response.contacs;

import com.moofwd.salesforce.response.StatusResponse;

public class ContactInformationResponse extends StatusResponse {
    private String contactId;
    private String accountId;

    public ContactInformationResponse() {
        this.contactId = "";
        this.accountId = "";
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
