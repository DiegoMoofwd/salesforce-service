package com.moofwd.salesforce.response.contacs;

import com.moofwd.salesforce.models.contacts.Contact;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class ContactResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<Contact> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<Contact> getRecords() {
        return records;
    }

    public void setRecords(List<Contact> records) {
        this.records = records;
    }
}
