package com.moofwd.salesforce.response.documents;

import com.moofwd.salesforce.models.documents.ContentDocumentId;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class ContentDocumentIdResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<ContentDocumentId> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<ContentDocumentId> getRecords() {
        return records;
    }

    public void setRecords(List<ContentDocumentId> records) {
        this.records = records;
    }
}
