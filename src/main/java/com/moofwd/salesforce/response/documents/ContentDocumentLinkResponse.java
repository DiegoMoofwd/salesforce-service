package com.moofwd.salesforce.response.documents;

import com.moofwd.salesforce.models.documents.ContentDocumentLink;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class ContentDocumentLinkResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<ContentDocumentLink> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<ContentDocumentLink> getRecords() {
        return records;
    }

    public void setRecords(List<ContentDocumentLink> records) {
        this.records = records;
    }
}
