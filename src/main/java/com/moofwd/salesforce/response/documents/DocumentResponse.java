package com.moofwd.salesforce.response.documents;

import com.moofwd.salesforce.models.documents.Document;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class DocumentResponse extends StatusResponse {
    private List<Document> attachments;

    public List<Document> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Document> attachments) {
        this.attachments = attachments;
    }
}