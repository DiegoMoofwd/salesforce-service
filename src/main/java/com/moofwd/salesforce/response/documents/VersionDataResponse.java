package com.moofwd.salesforce.response.documents;

import com.moofwd.salesforce.models.documents.VersionData;
import com.moofwd.salesforce.response.StatusResponse;

import java.util.List;

public class VersionDataResponse extends StatusResponse {
    private int totalSize;
    private boolean done;
    private List<VersionData> records;

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public List<VersionData> getRecords() {
        return records;
    }

    public void setRecords(List<VersionData> records) {
        this.records = records;
    }
}