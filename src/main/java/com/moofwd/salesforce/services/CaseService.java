package com.moofwd.salesforce.services;

import com.moofwd.salesforce.request.CaseIdRequest;
import com.moofwd.salesforce.request.cases.comments.AddCaseCommentRequest;
import com.moofwd.salesforce.request.cases.comments.CaseCommentRequest;
import com.moofwd.salesforce.request.cases.feeds.FeedItemRequest;
import com.moofwd.salesforce.request.documents.AddDocumentRequest;
import com.moofwd.salesforce.request.contacs.ContactRequest;
import com.moofwd.salesforce.request.cases.CreateCaseRequest;
import com.moofwd.salesforce.response.LoginResponse;
import com.moofwd.salesforce.response.cases.CasesResponse;
import com.moofwd.salesforce.response.CreateResponse;
import com.moofwd.salesforce.response.cases.comments.CaseCommentResponse;
import com.moofwd.salesforce.response.cases.feeds.FeedItemResponse;
import com.moofwd.salesforce.response.documents.ContentDocumentLinkResponse;
import com.moofwd.salesforce.response.documents.DocumentResponse;

public interface CaseService {
    LoginResponse getLoginResponse();

    CreateResponse createCase(CreateCaseRequest request);
    CasesResponse getCases(ContactRequest request);

    CreateResponse addAttachment(AddDocumentRequest request);
    DocumentResponse getAttachements(CaseIdRequest request);

    CreateResponse addCaseComment(AddCaseCommentRequest request);
    CaseCommentResponse getCaseCommentsByCaseId(CaseCommentRequest request);

    FeedItemResponse getFeedItemsByCaseId(FeedItemRequest request);
}
