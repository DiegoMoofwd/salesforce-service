package com.moofwd.salesforce.services.serviceImpl;

import com.moofwd.salesforce.models.cases.Case;
import com.moofwd.salesforce.models.cases.comments.feeds.FeedItem;
import com.moofwd.salesforce.models.contacts.Contact;
import com.moofwd.salesforce.models.documents.ContentDocumentLink;
import com.moofwd.salesforce.models.documents.Document;
import com.moofwd.salesforce.request.CaseIdRequest;
import com.moofwd.salesforce.request.cases.comments.AddCaseCommentRequest;
import com.moofwd.salesforce.request.cases.comments.CaseCommentRequest;
import com.moofwd.salesforce.request.cases.feeds.FeedItemRequest;
import com.moofwd.salesforce.request.documents.AddDocumentRequest;
import com.moofwd.salesforce.request.cases.CreateCaseRequest;
import com.moofwd.salesforce.request.contacs.ContactRequest;
import com.moofwd.salesforce.response.*;
import com.moofwd.salesforce.response.cases.CasesResponse;
import com.moofwd.salesforce.response.cases.comments.CaseCommentResponse;
import com.moofwd.salesforce.response.cases.feeds.FeedItemResponse;
import com.moofwd.salesforce.response.contacs.ContactInformationResponse;
import com.moofwd.salesforce.response.contacs.ContactResponse;
import com.moofwd.salesforce.response.documents.ContentDocumentIdResponse;
import com.moofwd.salesforce.response.documents.ContentDocumentLinkResponse;
import com.moofwd.salesforce.response.documents.DocumentResponse;
import com.moofwd.salesforce.response.documents.VersionDataResponse;
import com.moofwd.salesforce.services.CaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Service
public class CaseServiceImpl implements CaseService {

    @Value("${salesforce.instance}")
    private String salesforceInstance;

    @Value("${salesforce.version}")
    private String version;

    @Value("${salesforce.client_id}")
    private String clientId;

    @Value("${salesforce.client_secret}")
    private String clientSecret;

    @Value("${salesforce.username}")
    private String username;

    @Value("${salesforce.password}")
    private String password;

    @Autowired
    private RestTemplate clienteRest;

    @Override
    public LoginResponse getLoginResponse() {
        LoginResponse response = executeSalesForceLogin();
        return response;
    }

    @Override
    public CreateResponse createCase(CreateCaseRequest request) {
        CreateResponse createResponse = new CreateResponse();
        try{
            String token = request.getToken();

            //Get contact information or create a new contact if is not exist
            ContactInformationResponse contactInformationResponse = getUserContactInformation(request,"createCase", token);
            if(!contactInformationResponse.getStatus().equals("200")){
                createResponse.setStatus(contactInformationResponse.getStatus());
                createResponse.setMessage(contactInformationResponse.getMessage());
                return createResponse;
            }

            String contactId = contactInformationResponse.getContactId();
            String accountId = contactInformationResponse.getAccountId();
            //Validation that contactId or accountId are not empty
            if(contactId.equals("") || accountId.equals("")){
                createResponse.setStatus("400");
                createResponse.setMessage("Error al obtener contacto");
                return createResponse;
            }

            //Get Contact data of the contact who owns the case
            ContactResponse contactResponse = getContactByEmail(request.getOwnerEmail(), token);
            String ownerContactStatus = contactResponse.getStatus();
            if(!ownerContactStatus.equals("200") || contactResponse.getRecords().size() <= 0){
                createResponse.setStatus("400");
                createResponse.setMessage("Error al obtener contacto del usuario asignado al caso");
                return createResponse;
            }
            String ownerAccountId = contactResponse.getRecords().get(0).getAccountId();

            //Process of creation a new case
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);

            JSONObject contactJsonObject = new JSONObject();
            contactJsonObject.put("ContactId", contactId);
            contactJsonObject.put("AccountId", accountId);
            contactJsonObject.put("Status", "New");
            contactJsonObject.put("Origin", "MoofwdApp");
            contactJsonObject.put("Subject", request.getSubject());
            contactJsonObject.put("Type", request.getType());
            contactJsonObject.put("Description", request.getDescription());
            contactJsonObject.put("Colegio__c", ownerAccountId);
            //contactJsonObject.put("OwnerId", "0051I000002PtFaQAK");

            HttpEntity<String> entity = new HttpEntity<String>(contactJsonObject.toString(), headers);
            ResponseEntity<CreateResponse> response = clienteRest.postForEntity(salesforceInstance + "/services/data/"+ version +"/sobjects/Case", entity, CreateResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("201")){
                createResponse = response.getBody();
                createResponse.setStatus(status);
                createResponse.setMessage("Caso creado correctamente");
            }
        }catch (HttpClientErrorException ex){
            createResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            createResponse.setMessage("Error al crear caso");
        }catch (Exception ex){
            createResponse.setStatus("500");
            createResponse.setMessage(ex.getMessage());
        }
        return createResponse;
    }

    @Override
    public CasesResponse getCases(ContactRequest request) {
        CasesResponse casesResponse = new CasesResponse();
        try{
            String token = request.getToken();

            ContactInformationResponse contactInformationResponse = getUserContactInformation(request,"getCasesResponse", token);
            if(!contactInformationResponse.getStatus().equals("200")){
                casesResponse.setStatus(contactInformationResponse.getStatus());
                casesResponse.setMessage(contactInformationResponse.getMessage());
                return casesResponse;
            }

            String contactId = contactInformationResponse.getContactId();
            if(contactId.equals("")){
                casesResponse.setDone(true);
                casesResponse.setRecords(new ArrayList<>());
                casesResponse.setStatus(contactInformationResponse.getStatus());
                casesResponse.setMessage(contactInformationResponse.getMessage());
                return casesResponse;
            }

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            String queryString = "?q=SELECT+Id,+AccountId,+ParentId,+CaseNumber,+Origin,+Reason,+Type,+IsClosed,+ClosedDate,+Subject,+ContactEmail,+ContactId,+CreatedById,+CreatedDate,+Description,+SuppliedEmail,+IsEscalated,+(SELECT+id+FROM+CaseComments),+SuppliedName,+Status+FROM+Case+WHERE+ContactId='"+ contactId +"'+ORDER+BY+CreatedDate+DESC";
            ResponseEntity<CasesResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            CasesResponse.class);
            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("200")){
                casesResponse = response.getBody();
                casesResponse.setStatus(status);
                casesResponse.setMessage("Casos obtenidos correctamente");
                for (Case c : casesResponse.getRecords()) {
                    if(c.getCaseComments() != null){
                        c.getCaseComments().setStatus("200");
                        c.getCaseComments().setMessage("Comentarios obtenidos correctamente");
                    }
                }
            }
        }catch (HttpClientErrorException ex){
            casesResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            casesResponse.setMessage("Error al obtener casos");
        }catch (Exception ex){
            casesResponse.setStatus("500");
            casesResponse.setMessage(ex.getMessage());
        }
        return casesResponse;
    }

    @Override
    public CreateResponse addAttachment(AddDocumentRequest request) {
        CreateResponse createResponse = new CreateResponse();
        try{
            String status;
            String token = request.getToken();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);

            JSONObject contactJsonObject = new JSONObject();
            contactJsonObject.put("title", request.getTitle());
            contactJsonObject.put("VersionData", request.getVersionData());
            contactJsonObject.put("PathOnClient", request.getPathOnClient());
            contactJsonObject.put("ContentLocation", request.getContentLocation());

            HttpEntity<String> entity = new HttpEntity<String>(contactJsonObject.toString(), headers);
            ResponseEntity<CreateResponse> response = clienteRest.postForEntity(salesforceInstance + "/services/data/"+ version +"/sobjects/ContentVersion", entity, CreateResponse.class);

            status = String.valueOf(response.getStatusCode().value());
            if(!status.equals("201")){
                createResponse.setStatus(status);
                createResponse.setMessage("Error al adjuntar documento");
                return createResponse;
            }

            createResponse = response.getBody();
            createResponse.setStatus(status);
            createResponse.setMessage("Documento adjuntado correctamente");

            ContentDocumentIdResponse contentDocumentIdResponse = getContentDocument(createResponse.getId(), token);
            status = contentDocumentIdResponse.getStatus();
            if(!status.equals("200")){
                createResponse.setId("");
                createResponse.setStatus(status);
                createResponse.setSuccess(false);
                createResponse.setMessage(contentDocumentIdResponse.getMessage());
                return createResponse;
            }

            return contentDocumentLink(request.getCaseId(), contentDocumentIdResponse.getRecords().get(0).getContentDocumentId(), token); //Returns a create object
        }catch (HttpClientErrorException ex){
            createResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            createResponse.setMessage("Error al adjuntar documento");
        }catch (Exception ex){
            createResponse.setStatus("500");
            createResponse.setMessage(ex.getMessage());
        }
        return createResponse;
    }

    @Override
    public DocumentResponse getAttachements(CaseIdRequest request) {
        DocumentResponse documentResponse = new DocumentResponse();
        List<Document> documentList = new ArrayList<>();
        try{
            String token = request.getToken();
            ContentDocumentLinkResponse contentDocumentLinkResponse;

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            String queryString = "?q=SELECT+ContentDocumentId,+ContentDocument.Title+FROM+ContentDocumentLink+WHERE+LinkedEntityId='"+ request.getId() +"'";
            ResponseEntity<ContentDocumentLinkResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            ContentDocumentLinkResponse.class);
            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("200")){
                contentDocumentLinkResponse = response.getBody();
                for (ContentDocumentLink contentDocumentLink : contentDocumentLinkResponse.getRecords()) {
                    VersionDataResponse versionDataResponse = getVersionData(contentDocumentLink.getContentDocumentId(),token);
                    if(versionDataResponse.getStatus().equals("200") && versionDataResponse.getTotalSize() > 0){
                        Document doc = new Document();
                        doc.setName(contentDocumentLink.getContentDocument().getTitle());
                        doc.setUrl(salesforceInstance + versionDataResponse.getRecords().get(0).getVersionData());
                        doc.setAuthorization(token);
                        documentList.add(doc);
                    }
                }
                documentResponse.setAttachments(documentList);
                documentResponse.setStatus(status);
                documentResponse.setMessage("Documentos adjuntos obtenidos correctamente");
            }
        }catch (HttpClientErrorException ex){
            documentResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            documentResponse.setMessage("Error al obtener documentos adjuntos");
        }catch (Exception ex){
            documentResponse.setStatus("500");
            documentResponse.setMessage(ex.getMessage());
        }
        return documentResponse;
    }

    @Override
    public CreateResponse addCaseComment(AddCaseCommentRequest request) {
        CreateResponse createResponse = new CreateResponse();
        try{
            String token = request.getToken();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);

            JSONObject contactJsonObject = new JSONObject();
            contactJsonObject.put("CommentBody", request.getCommentBody());
            contactJsonObject.put("ParentId", request.getParentId());
            contactJsonObject.put("IsPublished", true);

            HttpEntity<String> entity = new HttpEntity<String>(contactJsonObject.toString(), headers);
            ResponseEntity<CreateResponse> response = clienteRest.postForEntity(salesforceInstance + "/services/data/"+ version +"/sobjects/CaseComment", entity, CreateResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if(!status.equals("201")){
                createResponse.setStatus(status);
                createResponse.setMessage("Error al agregar comentario");
                return createResponse;
            }

            createResponse = response.getBody();
            createResponse.setStatus(status);
            createResponse.setMessage("Comentario agregado correctamente");
        }catch (HttpClientErrorException ex){
            createResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            createResponse.setMessage("Error al agregar comentario");
        }catch (Exception ex){
            createResponse.setStatus("500");
            createResponse.setMessage(ex.getMessage());
        }
        return createResponse;
    }

    @Override
    public CaseCommentResponse getCaseCommentsByCaseId(CaseCommentRequest request) {
        CaseCommentResponse caseCommentResponse = new CaseCommentResponse();
        try{
            String token = request.getToken();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            ResponseEntity<CaseCommentResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/sobjects/Case/"+ request.getParentId() +"/CaseComments",
                            HttpMethod.GET,
                            entity,
                            CaseCommentResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if (status.equals("200")) {
                caseCommentResponse = response.getBody();
                caseCommentResponse.setStatus(status);
                caseCommentResponse.setMessage("Comentarios obtenidos correctamente");
            }
        }catch (HttpClientErrorException ex){
            caseCommentResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            caseCommentResponse.setMessage("Error al obtener comentarios");
        }catch (Exception ex){
            caseCommentResponse.setStatus("500");
            caseCommentResponse.setMessage(ex.getMessage());
        }
        return caseCommentResponse;
    }

    @Override
    public FeedItemResponse getFeedItemsByCaseId(FeedItemRequest request) {
        FeedItemResponse feedItemResponse = new FeedItemResponse();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + request.getToken());
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            String queryString = "?q=SELECT+ID,CreatedDate,CreatedById,CreatedBy.FirstName,CreatedBy.LastName,ParentId,Parent.Name,Body,+(SELECT Id,+CommentBody,+CommentType+FROM+FeedComments)+FROM+FeedItem+WHERE+ParentId='" + request.getParentId() + "'";
            ResponseEntity<FeedItemResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            FeedItemResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if (status.equals("200")) {
                feedItemResponse = response.getBody();
                feedItemResponse.setStatus(status);
                feedItemResponse.setMessage("Feed obtenido correctamente");

                for (FeedItem f : feedItemResponse.getRecords()) {
                    if(f.getFeedComments() != null){
                        f.getFeedComments().setStatus("200");
                        f.getFeedComments().setMessage("FeedComments obtenidos correctamente");
                    }
                }
            }
        } catch (HttpClientErrorException ex) {
            feedItemResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            feedItemResponse.setMessage("Error al obtener Feed del caso");
        } catch (Exception ex) {
            feedItemResponse.setStatus("500");
            feedItemResponse.setMessage(ex.getMessage());
        }
        return feedItemResponse;
    }

    private VersionDataResponse getVersionData(String contentDocumentId, String token) {
        VersionDataResponse versionDataResponse = new VersionDataResponse();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            String queryString = "?q=SELECT+VersionData+FROM+ContentVersion+WHERE+ContentDocumentId='" + contentDocumentId + "'";
            ResponseEntity<VersionDataResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            VersionDataResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if (status.equals("200")) {
                versionDataResponse = response.getBody();
                versionDataResponse.setStatus(status);
                versionDataResponse.setMessage("Versión del documento obtenida correctamente");
            }
        } catch (HttpClientErrorException ex) {
            versionDataResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            versionDataResponse.setMessage("Error al obtener versión del documento");
        } catch (Exception ex) {
            versionDataResponse.setStatus("500");
            versionDataResponse.setMessage(ex.getMessage());
        }
        return versionDataResponse;
    }

    private ContentDocumentIdResponse getContentDocument(String contentVersionId, String token) {
        ContentDocumentIdResponse contentDocumentIdResponse = new ContentDocumentIdResponse();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            String queryString = "?q=SELECT+ContentDocumentId+FROM+ContentVersion+WHERE+Id='" + contentVersionId + "'";
            ResponseEntity<ContentDocumentIdResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            ContentDocumentIdResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if (status.equals("200")) {
                contentDocumentIdResponse = response.getBody();
                contentDocumentIdResponse.setStatus(status);
                contentDocumentIdResponse.setMessage("Documento obtenido correctamente");
            }
        } catch (HttpClientErrorException ex) {
            contentDocumentIdResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            contentDocumentIdResponse.setMessage("Error al obtener documento");
        } catch (Exception ex) {
            contentDocumentIdResponse.setStatus("500");
            contentDocumentIdResponse.setMessage(ex.getMessage());
        }
        return contentDocumentIdResponse;
    }

    private CreateResponse contentDocumentLink(String caseId, String contentDocumentId, String token){
        CreateResponse createResponse = new CreateResponse();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);

            JSONObject contactJsonObject = new JSONObject();
            contactJsonObject.put("LinkedEntityId", caseId);
            contactJsonObject.put("Visibility", "AllUsers");
            contactJsonObject.put("ContentDocumentId", contentDocumentId);

            HttpEntity<String> entity = new HttpEntity<String>(contactJsonObject.toString(), headers);
            ResponseEntity<CreateResponse> response = clienteRest.postForEntity(salesforceInstance + "/services/data/"+ version +"/sobjects/ContentDocumentLink", entity, CreateResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("201")){
                createResponse = response.getBody();
                createResponse.setStatus(status);
                createResponse.setMessage("Documento asociado correctamente");
            }
        }catch (HttpClientErrorException ex){
            createResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            createResponse.setMessage("Error al asociar documento");
        }catch (Exception ex){
            createResponse.setStatus("500");
            createResponse.setMessage(ex.getMessage());
        }
        return createResponse;
    }

    private ContactInformationResponse getUserContactInformation(ContactRequest contact, String type, String token){
        ContactInformationResponse contactInformationResponse = new ContactInformationResponse();
        try{
            ContactResponse contactResponse = getContactByEmail(contact.getUserEmail(), token);
            if(contactResponse.getStatus().equals("200")){
                if(contactResponse.getTotalSize() > 0){ //I found at least one contact, so I return the first contact of the ArrayList
                    contactInformationResponse.setStatus("200");
                    contactInformationResponse.setMessage("Contacto encontrado");
                    contactInformationResponse.setContactId(contactResponse.getRecords().get(0).getId());
                    contactInformationResponse.setAccountId(contactResponse.getRecords().get(0).getAccountId());
                    return contactInformationResponse;
                }

                //I don't found a Contact, then
                if(type.equals("createCase")){
                    //I need to create a new Contact:
                    CreateResponse createResponse = createContact(contact, token);
                    if(createResponse.getStatus().equals("201")){
                        contactInformationResponse.setStatus("200");
                        contactInformationResponse.setMessage(createResponse.getMessage());
                        contactInformationResponse.setContactId(createResponse.getId());
                        contactInformationResponse.setAccountId(contact.getAccountId());

                        /*contactResponse = getAccountIdByEmail(contact.getUserEmail(), token);
                        status = contactResponse.getStatus();
                        if(status.equals("200")){
                            contactInformationResponse.setAccountId(contactResponse.getRecords().get(0).getAccountId());
                        }*/
                    }else{
                        //I have an error creating a new account
                        contactInformationResponse.setStatus(createResponse.getStatus());
                        contactInformationResponse.setMessage(createResponse.getMessage());
                        contactInformationResponse.setContactId("");
                        contactInformationResponse.setAccountId("");
                    }
                }else{
                    //Else we returns contact doesn't exist
                    contactInformationResponse.setStatus("400");
                    contactInformationResponse.setMessage("Contacto no existe en Salesforce");
                    contactInformationResponse.setContactId("");
                    contactInformationResponse.setAccountId("");
                }
            }
        }catch (HttpClientErrorException ex){
            contactInformationResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            contactInformationResponse.setMessage("Error al obtener contacto");
        }catch (Exception ex){
            contactInformationResponse.setStatus("500");
            contactInformationResponse.setMessage(ex.getMessage());
        }
        return contactInformationResponse;
    }

    private ContactResponse getContactByEmail(String email, String token){
        ContactResponse contactResponse = new ContactResponse();
        try{
            String status;

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            //Get contact information by email
            String queryString = "?q=SELECT+Id,+AccountId,+Email+FROM+Contact+WHERE+Email='"+ email +"'";
            ResponseEntity<ContactResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            ContactResponse.class);

            status = String.valueOf(response.getStatusCode().value());
            if(status.equals("200")){
                //Always validate that records > 0
                contactResponse = response.getBody();
                contactResponse.setMessage("");
            }else{
                contactResponse.setMessage("Error al obtener contacto");
            }
            contactResponse.setStatus(status);

        }catch (HttpClientErrorException ex){
            contactResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            contactResponse.setMessage("Error al obtener contacto");
        }catch (Exception ex){
            contactResponse.setStatus("500");
            contactResponse.setMessage(ex.getMessage());
        }
        return contactResponse;
    }

    //Deprecated
    private ContactResponse getAccountIdByEmail(String email, String token){
        ContactResponse contactResponse = new ContactResponse();
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            String queryString = "?q=SELECT+AccountId+FROM+Contact+WHERE+Email='"+ email +"'";
            ResponseEntity<ContactResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/data/"+ version +"/query/" + queryString,
                            HttpMethod.GET,
                            entity,
                            ContactResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("200")){
                contactResponse = response.getBody();
                if(contactResponse.getTotalSize() > 0){
                    //I found at least one Contact
                    contactResponse.setStatus(status);
                    contactResponse.setMessage("accountId obtenido correctamente");
                }
            }
        }catch (HttpClientErrorException ex){
            contactResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            contactResponse.setMessage("Error al obtener accountId");
        }catch (Exception ex){
            contactResponse.setStatus("500");
            contactResponse.setMessage(ex.getMessage());
        }
        return contactResponse;
    }
    //Deprecated

    private CreateResponse createContact(ContactRequest contact, String token) {
        CreateResponse createResponse = new CreateResponse();
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);

            JSONObject contactJsonObject = new JSONObject();
            contactJsonObject.put("FirstName", contact.getUserFirstName());
            contactJsonObject.put("LastName", contact.getUserLastName());
            contactJsonObject.put("Email", contact.getUserEmail());
            contactJsonObject.put("AccountId", contact.getAccountId());

            HttpEntity<String> entity = new HttpEntity<String>(contactJsonObject.toString(), headers);
            ResponseEntity<CreateResponse> response = clienteRest.postForEntity(salesforceInstance + "/services/data/"+ version +"/sobjects/Contact", entity, CreateResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("201")){
                createResponse = response.getBody();
                createResponse.setStatus(status);
                createResponse.setMessage("Contacto creado correctamente");
            }
        }catch (HttpClientErrorException ex){
            createResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            createResponse.setMessage("Error al crear contacto");
        }catch (Exception ex){
            createResponse.setStatus("500");
            createResponse.setMessage(ex.getMessage());
        }
        return createResponse;
    }

    private LoginResponse executeSalesForceLogin() {
        LoginResponse loginResponse = new LoginResponse();
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("grant_type", "password");
            map.add("client_id", clientId);
            map.add("client_secret", clientSecret);
            map.add("username", username);
            map.add("password", password);

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

            ResponseEntity<LoginResponse> response =
                    clienteRest.exchange(salesforceInstance + "/services/oauth2/token",
                            HttpMethod.POST,
                            entity,
                            LoginResponse.class);

            String status = String.valueOf(response.getStatusCode().value());
            if(status.equals("200")){
                loginResponse = response.getBody();
                loginResponse.setStatus(status);
                loginResponse.setMessage("Autenticación realizada correctamente");
            }
        }catch (HttpClientErrorException ex){
            loginResponse.setStatus(String.valueOf(ex.getRawStatusCode()));
            loginResponse.setMessage("Error al realizar autenticación");
        }catch (Exception ex){
            loginResponse.setStatus("500");
            loginResponse.setMessage(ex.getMessage());
        }
        return loginResponse;
    }

}